public class OpenWeatherCalloutService {

    private static final String ENDPOINT_URL = 'http://api.openweathermap.org/data/2.5/weather?id=';
    private static final String API_KEY = '5477c643e3243e7a9567d74000312daf';
    private static Map<String, City__c> cityIdByCityCode;

    public static void getWeatherFromOpenWeather(){
        cityIdByCityCode = new Map<String, City__c> ();
        for(City__c city : [SELECT Id, City_Code__c FROM City__c]){
            cityIdByCityCode.put(city.City_Code__c, city);
        }
        List<Weather__c> weathers = new List<Weather__c> ();
        for(City__c city : cityIdByCityCode.values()){
            Weather__c weather = makeCallout(city.City_Code__c);
            if(weather != null){
                weathers.add(weather);
            }
        }
        if(!weathers.isEmpty()){
            insert weathers;
        }
    }

    public static Weather__c makeCallout(String cityId){
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(ENDPOINT_URL + cityId + '&appid=' + API_KEY);
        request.setMethod('GET');
        HttpResponse response = http.send(request);

        if(response.getStatusCode() == 200){
            OpenWeatherResponse weatherResponse =
                    (OpenWeatherResponse) JSON.deserialize(response.getBody(), OpenWeatherResponse.class);
            return getWeatherFromResponse(weatherResponse, cityId);

        } else {
            System.debug('Status code: ' + response.getStatusCode());
            System.debug('Status code: ' + response.getStatus());
            return null;
        }
    }

    public static Weather__c getWeatherFromResponse(OpenWeatherResponse response, String cityId){

        Weather__c weather = DataFactory.getWeather(
                cityIdByCityCode.get(cityId),
                response.main.temp,
                response.main.feels_like,
                response.main.temp_min,
                response.main.temp_max,
                response.main.pressure,
                response.main.humidity);
        return weather;
    }

    /*
    * TODO: implementation fields:
    *   - visibility
    *   - wind
    *   - clouds
    *   - sys
    *   - timezone
    *   - name
    **/
    public class OpenWeatherResponse {
        public Main main;
        public Integer visibility;
        public Wind wind;
        public Clouds clouds;
//        public Sys sys;
//        public Integer timezone;
//        public String name;
    }

    public class Main {
        public Decimal temp;
        public Decimal feels_like;
        public Decimal temp_min;
        public Decimal temp_max;
        public Integer pressure;
        public Integer humidity;
    }

    public class Clouds {
        public Integer all;
    }

    public class Wind {
        public Decimal speed;
//        public Integer deg;
    }

//    public class Sys {
//        Id id;
//        String country;
//        Integer sunrise;
//        Integer sunset;
//    }

}
