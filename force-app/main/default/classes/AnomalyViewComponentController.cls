public without sharing class AnomalyViewComponentController {

    public static void generateReport(){
        Anomaly_Report__c report = new Anomaly_Report__c(Name = 'Anomaly Report ' + System.now().format('yyyy.MM.dd h:mm a'));
        insert report;
        ContentVersion cv = new ContentVersion();
        cv.Title = report.Name;
        cv.PathOnClient = cv.Title + '.pdf';
        cv.IsMajorVersion = true;
        cv.FirstPublishLocationId = report.Id;
        cv.VersionData = Page.AnomalyReportPage.getContentAsPDF();
        insert cv;
    }
}
