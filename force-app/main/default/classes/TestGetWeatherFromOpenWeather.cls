@IsTest
private class TestGetWeatherFromOpenWeather {

    @IsTest
    static void testScheduledJob() {
        Test.startTest();
        String jobId = GetWeatherFromOpenWeather.runGetWeatherFromOpenWeather();
        CronTrigger job =  [SELECT Id FROM CronTrigger WHERE Id = :jobId];
        Test.stopTest();

        System.assertEquals(jobId, job.Id, 'Tasks were not created');
    }
}