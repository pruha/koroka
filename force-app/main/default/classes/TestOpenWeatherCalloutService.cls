@IsTest
private class TestOpenWeatherCalloutService {

    static public String body = '{"main":{"temp":282.55, "feels_like":281.86, "temp_min":280.37, "temp_max":284.26, "pressure":1023,"humidity":100},' +
                                     '"visibility":16093,' +
                                     '"wind":{"speed":1.5},' +
                                     '"clouds":{"all":1}}';
    static public String cityName = 'Test city';

    @TestSetup
    static void makeData(){

        City__c city = TestDataFactory.createCity(cityName);
        insert city;
    }

    @IsTest
    static void isPositiveCallout() {

        Integer trueCode = 200;
        HttpMockFactory mock = new HttpMockFactory(trueCode, 'OK', body, new Map<String,String>());
        Test.setMock(HttpCalloutMock.class, mock);

        Test.startTest();
        OpenWeatherCalloutService.getWeatherFromOpenWeather();
        Test.stopTest();

        List<Weather__c> weathers = [SELECT Id FROM Weather__c];
        List<City__c> cities = [SELECT Id FROM City__c];

        System.assert(weathers.size() == cities.size());
        System.assert(weathers.size() == 1);
        System.assert(cities.size() == 1);
    }

    @IsTest
    static void isNegativeCallout() {

        Integer falseCode = 400;
        HttpMockFactory mock = new HttpMockFactory(falseCode, 'OK', body, new Map<String,String>());
        Test.setMock(HttpCalloutMock.class, mock);

        Test.startTest();
        OpenWeatherCalloutService.getWeatherFromOpenWeather();
        Test.stopTest();

        List<Weather__c> weathers = [SELECT Id FROM Weather__c];
        List<City__c> cities = [SELECT Id FROM City__c];

        System.assert(weathers.size() != cities.size());
        System.assert(weathers.size() == 0);
        System.assert(cities.size() == 1);
    }
}